FROM 32bit/ubuntu:16.04

# Setup repositories
# RUN apt-get install -y software-properties-common
# RUN add-apt-repository -y ppa:george-edison55/cmake-3.x

# Setup packages
RUN dpkg --add-architecture i386 && apt-get update 
RUN apt-get install -y openssl openssh-server git gcc g++ make cmake zip bash-completion ruby xz-utils debhelper devscripts libc6-dev
RUN apt-get install -y libc6-dev:i386 libasound2:i386 libasound2-dev:i386 libasound2-plugins:i386 libssl-dev:i386 libssl1.0.2:i386 libfreetype6-dev:i386 libx11-dev:i386 libsm-dev:i386 libice-dev:i386
RUN apt-get install -y build-essential gcc-multilib g++ libxext-dev
RUN apt-get install -y libgl1-mesa-dev libgl1-mesa-glx:i386 mesa-common-dev
# RUN apt-get install -y dosfstools u-boot-tools mtools grub-pc grub
RUN apt-get install -y dosfstools u-boot-tools mtools grub

# Setup XEN
RUN apt-get install -y m4 ocaml ocaml-native-compilers camlp4-extra

# Init services
# RUN service ssh start

CMD ["/bin/bash"]
